const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//register
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		yourName: reqBody.yourName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
};

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if (result.length > 0) {
			return true
		} else {
			return false
		}

	})
}

//login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}

	})
}

module.exports.getProfile = (userData) => {

	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		console.log(result)

		result.password = "*****";

		// Returns the user information with the password as an empty string
		return result;

		}
	});

};

//order