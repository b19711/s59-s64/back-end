const Product = require('../models/Product');
// const auth = require('../auth');
const User = require('../models/User');



//add product
module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then((result)=>{

        if(!userData.isAdmin){
            return false
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                category: reqBody.category,
                price: reqBody.price
            });

            return newProduct.save().then((product, error)=> {
               
                if(error){
                    return false
                } else {
                    return true
                }
            })
        }
    })
};


//get all active product
 module.exports.getAllActive = () => {
    return Product.find({isActive : true})
     
    
 };
module.exports.getAllProducts = (data) => {
    if (data.isAdmin) {
        return Product.find({}).then(result => {

        return result
    })
} else {
    return false
}

         
        
     };

//retrieve single product
module.exports.getProduct = (reqParams) =>{
    return Product.findById(reqParams.productId)
    .then(result =>{
        return result
    })
};



//update product
module.exports.updateProduct = async (reqParams, reqBody, userData) =>{
   
    if(userData.isAdmin === true) {
        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            category: reqBody.category,
            price: reqBody.price

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((updatedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
       return await false
    }
};

//Archive Product S44
module.exports.archiveProduct = async (userData, reqBody) =>{
    if(userData.isAdmin === true) {

        let archivedProduct = {
           isActive: reqBody.isActive

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(userData.productId,
            archivedProduct).then((archivedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
        return await false
    }
}



module.exports.activeProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.isAdmin === true) {

			result.isActive = true;

			return result.save().then((activeProduct, err) => {

				// Course not activated
				if(err) {

					return false;

				// Course activated successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}
