const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');
// const userController = require('../controllers/userController');


// add product
router.post('/', auth.verify, (req,res)=> {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin})
    .then(resultFromController => 
        res.send(resultFromController))
});


//get all product
router.get('/', (req,res)=>{
    productController.getAllActive()
    .then(resultFromController => res.send(resultFromController))
});


//get specific Product
router.get("/:productId", (req,res)=>{
    console.log(req.params.productId);
    productController.getProduct(req.params)
    .then(resultFromController => 
        res.send(resultFromController))
})


//update product
router.put('/:productId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = auth.decode(req.headers.authorization)
    console.log(isAdminData)

    productController.updateProduct(req.params, req.body, isAdminData)
    .then(resultFromController => 
        res.send(resultFromController))
});


//archive product
router.put('/archive/:productId', auth.verify, (req,res)=>{
    //auth
    const isAdminData = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(isAdminData, req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});

//Updated for Frontend
router.put('/active/:productId', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.activeProduct(data).then(resultFromController => res.send(resultFromController))
});




module.exports = router